//
//  MyFreebieCell.h
//  CoffeeApp
//
//  Created by vairat on 15/06/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyFreebieCell : UICollectionViewCell


@property(nonatomic, strong)IBOutlet UIView *cellBackground_View;
@property(nonatomic, strong)IBOutlet UILabel *productName_Label;
@property(nonatomic, strong)IBOutlet UIImageView *freebie_ImageView;
@end
