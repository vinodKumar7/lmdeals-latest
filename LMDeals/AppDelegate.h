//
//  AppDelegate.h
//  LMDeals
//
//  Created by vairat on 20/06/15.
//  Copyright (c) 2015 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Product.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (BOOL) isIphone5;

// My Favorite methods
- (BOOL) addProductToFavorites:(Product *) product;
- (BOOL) removeProductFromfavorites:(Product *) product;
- (BOOL) productExistsInFavorites:(Product *) product;
- (NSArray *) myFavoriteProducts;

@end

