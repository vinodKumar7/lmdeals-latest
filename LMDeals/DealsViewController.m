//
//  DealsViewController.m
//  LMDeals
//
//  Created by vairat on 22/06/15.
//  Copyright (c) 2015 vairat. All rights reserved.
//

#import "DealsViewController.h"
#import "MyFreebieCell.h"
#import "DealDetailViewController.h"
#import "ASIFormDataRequest.h"
#import "DealsTableCell.h"

#define DegreesToRadians(x) ((x) * M_PI / 180.0)

@interface DealsViewController (){
    
    ProductListParser  *productsXMLParser;
    UIImage *fetchedProductImage;
    Product *pro;
    UIView *menuView;
    
    UITableView *deals_tableView;
    MKMapView *myMapView;
    
    CGPoint point;
    BOOL expand;
}
@property(nonatomic, strong)UIImage *fetchedProductImage;
@property (nonatomic, strong)UITableView *deals_tableView;

@end

@implementation DealsViewController
@synthesize deals_CollectionView,productsList,fetchedProductImage;
@synthesize deals_tableView;
@synthesize view1, view2, view3;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Deals";
    
    productImagesArray = [[NSMutableArray alloc]init];
    
    if ([self.navigationController.parentViewController respondsToSelector:@selector(revealGesture:)] && [self.navigationController.parentViewController respondsToSelector:@selector(revealToggle:)])
    {
        UIPanGestureRecognizer *navigationBarPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.navigationController.parentViewController action:@selector(revealGesture:)];
        [self.navigationController.navigationBar addGestureRecognizer:navigationBarPanGestureRecognizer];
        
        
        self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"ButtonMenu"]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self.navigationController.parentViewController action:@selector(revealToggle:)];
        
       
        
    }
    self.navigationController.navigationBar.translucent = NO;
    
//    UIBarButtonItem *flipButton = [[UIBarButtonItem alloc]
//                                   initWithTitle:@"View"
//                                   style:UIBarButtonItemStylePlain
//                                   target:self
//                                   action:@selector(menu)];
   // self.navigationItem.rightBarButtonItem = flipButton;
    
    
    //-------
    
     menuView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 60)];
     menuView.backgroundColor = [UIColor whiteColor];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *bluredEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    [bluredEffectView setFrame:menuView.bounds];
    
    [menuView addSubview:bluredEffectView];
    
    UISegmentedControl *segmentControl = [[UISegmentedControl alloc]initWithItems:@[@"Grid",@"List",@"Map"]];
    
  
    segmentControl.frame = CGRectMake(10, 20, 300, 30);
    [segmentControl addTarget:self action:@selector(segmentedControlValueDidChange:) forControlEvents:UIControlEventValueChanged];
    [segmentControl setSelectedSegmentIndex:0];
    [menuView addSubview:segmentControl];
    
    
    
    
    
    //------
   
   
    
    
    CGRect frame = CGRectMake(0, 0, 320, 54);
    frame.origin.y = CGRectGetMaxY(self.navigationController.navigationBar.frame) - frame.size.height;
    menuView.frame = frame;
    
    [self.navigationController.navigationBar.superview insertSubview:menuView belowSubview:self.navigationController.navigationBar];
    
    
    
    
    
    [self.deals_CollectionView registerNib:[UINib nibWithNibName:@"MyFreebieCell" bundle:nil] forCellWithReuseIdentifier:@"MyFreebieCell"];
    
    
    [self createTableView];
    [self createMkMapView];
    [self fetchDeals];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    expand = NO;
    
    
    self.view1 = [[UIView alloc]initWithFrame:CGRectMake(0, 8, 25, 3)];
    self.view2 = [[UIView alloc]initWithFrame:CGRectMake(0, 15, 25, 3)];
    self.view3 = [[UIView alloc]initWithFrame:CGRectMake(0, 22, 25, 3)];
    
    
    self.view1.backgroundColor = [UIColor orangeColor];
    self.view2.backgroundColor = [UIColor orangeColor];
    self.view3.backgroundColor = [UIColor orangeColor];
    
    
    
    UIView *rightMenuView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    rightMenuView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapAction:)];
    [rightMenuView addGestureRecognizer:tapRecognizer];
    
    [rightMenuView addSubview:self.view1];
    [rightMenuView addSubview:self.view2];
    [rightMenuView addSubview:self.view3];
    
    
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightMenuView];
    
    self.navigationItem.rightBarButtonItem = barButtonItem;
    //[[[self navigationController] navigationItem] setRightBarButtonItem:barButtonItem];
    
    
    
    
    
    
    
    
    // NSLog(@"center X: %f",view1.layer.)
    NSLog(@"%f %f",view1.frame.origin.x,view1.frame.origin.y);
    NSLog(@"%f %f",view2.frame.origin.x,view2.frame.origin.y);
    NSLog(@"----------------------------");
    
    // self.view1.layer.anchorPoint = CGPointMake(0.5, 0.5);
    // self.view2.layer.anchorPoint = CGPointMake(0.5, 0.5);
    NSLog(@"%f %f",self.view1.center.x,self.view1.center.y);
    
    point = CGPointMake(self.view2.center.x, self.view2.center.y);
}

- (void)onTapAction:(UITapGestureRecognizer*)recognizer
{
    
    if(!expand)
    {
        [UIView animateWithDuration:0.20
                         animations:^{
                             view1.transform = CGAffineTransformMakeRotation(DegreesToRadians(48));
                             view2.transform = CGAffineTransformMakeRotation(-DegreesToRadians(48));
                             
                             
                             self.view2.center = self.view1.center;
                             view3.alpha = 0;
                         }
                         completion:^(BOOL finished){
                             expand = YES;
                         }];
        [self segmentedControlMoveDown];
        
    }
    else
    {
        [UIView animateWithDuration:0.25
                         animations:^{
                             
                             view1.transform = CGAffineTransformIdentity;
                             view2.transform = CGAffineTransformIdentity;
                             
                             self.view2.center = point;
                             view3.alpha = 1;
                         }
                         completion:^(BOOL finished){
                             
                             expand = NO;
                             
                         }];
        
        [self segmentedControlMoveUP];
    }
    
    
}

-(void)segmentedControlMoveDown
{
    [UIView animateWithDuration:0.5 delay:0
         usingSpringWithDamping:0.4 initialSpringVelocity:0.0f
                        options:0 animations:^{
                            // primaryConstraint.constant = 0;
                            CGRect frame = menuView.frame;
                            frame.origin.y = CGRectGetMaxY(self.navigationController.navigationBar.frame)-10;
                            menuView.frame = frame;
                            [self.view layoutIfNeeded];
                        } completion:nil];
}
-(void)segmentedControlMoveUP
{
    [UIView animateWithDuration:0.5 delay:0
         usingSpringWithDamping:0.4 initialSpringVelocity:0.0f
                        options:0 animations:^{
                            // primaryConstraint.constant = 0;
                            CGRect frame = menuView.frame;
                            frame.origin.y = 0;
                            menuView.frame = frame;
                            [self.view layoutIfNeeded];
                        } completion:nil];
}

-(void)createTableView
{
    deals_tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height -65) style:UITableViewStylePlain];
   
    deals_tableView.dataSource = self;
    deals_tableView.delegate = self;
    deals_tableView.separatorStyle = NO;
    
    [self.view addSubview:deals_tableView];
    
    deals_tableView.hidden = YES;
}

-(void)createMkMapView
{
    myMapView = [[MKMapView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:myMapView];

    myMapView.hidden = YES;
}

-(void)fetchDeals{
      NSString *urlString =@"http://184.107.152.53/app/webroot/newapp/get_loyalty_offers.php?cid=24";
      NSURL *url = [NSURL URLWithString:urlString];

      ASIFormDataRequest *productFetchRequest = [ASIFormDataRequest requestWithURL:url];
      [productFetchRequest setDelegate:self];
      [productFetchRequest startAsynchronous];

}



- (void)segmentedControlValueDidChange:(UISegmentedControl *)segment
{
    if(segment.selectedSegmentIndex == 0)
    {
        NSLog(@"Grid");
        
        deals_tableView.hidden = YES;
        myMapView.hidden = YES;
        deals_CollectionView.hidden = NO;
        
    }
    else if(segment.selectedSegmentIndex == 1){
        NSLog(@"List");
        
        deals_CollectionView.hidden = YES;
        myMapView.hidden = YES;
        deals_tableView.hidden = NO;
        
    }
    else{
        NSLog(@"map");
        deals_CollectionView.hidden = YES;
        deals_tableView.hidden = YES;
        myMapView.hidden = NO;
    }
}








- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
    productsXMLParser = [[ProductListParser alloc] init];
    productsXMLParser.delegate = self;
    productsParser.delegate = productsXMLParser;
    [productsParser parse];
    
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:@"Server Busy"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [alert_view show];
    
    
}

#pragma mark- Calling Parsing Methods
- (void)parsingProductListFinished:(NSArray *)prodcutsListLocal
{
    NSLog(@"product name %@",prodcutsListLocal);
    
    if (!productsList)
        
        productsList = [[NSMutableArray alloc] initWithArray:prodcutsListLocal];
    
    else
        
        [productsList addObjectsFromArray:prodcutsListLocal];
    
    
    [self.deals_CollectionView reloadData];
    [deals_tableView reloadData];
    
}

- (void)parsingProductListXMLFailed
{
    //NSLog(@"Product list updated failed ");
}




-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [productsList count];
}

-(MyFreebieCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *cellIdentifier = @"MyFreebieCell";
    MyFreebieCell *cell;
    
     if (cell == nil)
         cell = (MyFreebieCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    //cell.freebie_ImageView.layer.borderColor  = [UIColor blackColor].CGColor;
    // cell.freebie_ImageView.layer.borderWidth  = 1;
    //cell.freebie_ImageView.layer.masksToBounds = YES;
   // cell.freebie_ImageView.layer.cornerRadius = 3.5;
    
    cell.tag = indexPath.row;
    pro = [productsList objectAtIndex:indexPath.row];
    cell.productName_Label.text = pro.productName;
    
    cell.freebie_ImageView.image = nil;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:pro.carouselImgLink]];
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (cell.tag == indexPath.row) {
                    cell.freebie_ImageView.image = image;
                    //[productImagesArray addObject:image];
                    // NSLog(@"count of product image is %d ",productImagesArray.count);
                    [cell setNeedsLayout];
                }
            });
        }
    });

    
    
//    customBadge = [CustomBadge customBadgeWithString:@"5"];
//    badgeView = [[UIView alloc]initWithFrame:CGRectMake(75, 0, 28, 28)];
//    badgeView.backgroundColor = [UIColor clearColor];
//    [badgeView addSubview:customBadge];
//    [cell.cellBackground_View addSubview:badgeView];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath

{
    
    NSLog(@"didSelectItemAtIndexPath");
    
    [self segmentedControlMoveUP];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    DealDetailViewController *dealDetailViewController = [storyboard instantiateViewControllerWithIdentifier:@"DealDetailViewController"];
    
    [self.navigationController pushViewController:dealDetailViewController animated:YES];
    
    
}

#pragma mark - TableView DataSource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [productsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reusableIdentifier = @"cell";
    
    //    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
    
    DealsTableCell *cell = (DealsTableCell *)[tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
    
    if (!cell) {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"DealsTableCell" owner:nil options:nil];
        
        for (UIView *cellview in views)
        {
            if([cellview isKindOfClass:[UITableViewCell class]])
            {
                cell = (DealsTableCell*)cellview;
            }
        }
        
    }
    
    cell.tag = indexPath.row;
    pro = [productsList objectAtIndex:indexPath.row];
    cell.productNameLabel.text = pro.productName;
//    cell.productImgView.image = [productImagesArray objectAtIndex:indexPath.row];
    
    cell.productImgView.image = nil;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:pro.carouselImgLink]];
        NSLog(@"CarouselImageLink is .....%@",pro.carouselImgLink);
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (cell.tag == indexPath.row) {
                    cell.productImgView.image = image;
                    //[productImagesArray addObject:image];
                    // NSLog(@"count of product image is %d ",productImagesArray.count);
                    [cell setNeedsLayout];
                }
            });
        }
    });
    
    
    int rowModuleNo = indexPath.row % 2;
    
    if (rowModuleNo == 0)
        cell.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
        
    else if (rowModuleNo == 1)
        cell.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];
  
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90.0;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
