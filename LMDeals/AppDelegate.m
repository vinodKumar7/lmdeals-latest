//
//  AppDelegate.m
//  LMDeals
//
//  Created by vairat on 20/06/15.
//  Copyright (c) 2015 vairat. All rights reserved.
//

#import "AppDelegate.h"
#import "RevealController.h"
#import "HomeViewController.h"
#import "SearchViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    HomeViewController *homeVC = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    SearchViewController *searchVC = [[SearchViewController alloc]initWithNibName:@"SearchViewController" bundle:nil];
    
    
   
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:homeVC];
    
    RevealController *revealController = [[RevealController alloc] initWithFrontViewController:navigationController rearViewController:searchVC];
    
    self.window.rootViewController = revealController;
    [self.window makeKeyAndVisible];
    return YES;

}
- (BOOL) isIphone5 {
    
    if( [ [ UIScreen mainScreen ] bounds ].size.height == 568.0){
        
        NSLog(@"====IPHONE5====");
        return YES;
    }
    NSLog(@"====IPHONE4====");
    return NO;
}
#pragma mark -- Favorite methods

- (BOOL) addProductToFavorites:(Product *) product {
    
    if ([self productExistsInFavorites:product]) {
        // Product already exists. return.
        return NO;
    }
    
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *favoriteProduct = [NSEntityDescription insertNewObjectForEntityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [favoriteProduct setValue:product.productId forKey:@"productId"];
    [favoriteProduct setValue:product.productName forKey:@"productName"];
    [favoriteProduct setValue:product.productOffer forKey:@"productHighlight"];
    [favoriteProduct setValue:@"1" forKey:@"userId"];
    
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Adding product failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}

- (BOOL) removeProductFromfavorites:(Product *) product {
    
    if ([self productExistsInFavorites:product]) {
        NSManagedObjectContext *context = [self managedObjectContext];
        
        // Fetch the said NSManagedObject(favoriteProduct)
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
        [fetchRequest setEntity:favProduct];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productId == %@ && userId==1",product.productId];
        [fetchRequest setPredicate:predicate];
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        if (fetchedObjects.count > 0) {
            NSManagedObject *favoriteProduct = [fetchedObjects objectAtIndex:0];
            
            [context deleteObject:favoriteProduct];
            
            NSError *error;
            if (![context save:&error]) {
                NSLog(@"Removing product failed %@", [error localizedDescription]);
                
                // Removing object failed.
                return NO;
            }
            
            // Success
            return YES;
        }
        
        // There is no Product as such specified by input param in favorites.
    }
    
    // There is no Product as such specified by input param in favorites.
    return NO;
}



- (BOOL) productExistsInFavorites:(Product *) product {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productId==%@ && userId==1",product.productId];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0) {
        return YES;
    }
    
    return NO;
}

- (NSArray *) myFavoriteProducts {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId==1"];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *productsArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects) {
        NSLog(@"Product Name: %@", [info valueForKey:@"productName"]);
        NSLog(@"Product Id: %@", [info valueForKey:@"productId"]);
        
        Product *pr = [[Product alloc] init];
        pr.productId = [[info valueForKey:@"productId"] copy];
        pr.productName = [[info valueForKey:@"productName"] copy];
        pr.productOffer = [[info valueForKey:@"productHighlight"] copy];
        [productsArr addObject:pr];
    }
    
    return productsArr;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "vairat.LMDeals" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
   // NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"LMDeals" withExtension:@"momd"];
   // _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"LMDeals.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
