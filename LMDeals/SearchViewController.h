//
//  SearchViewController.h
//  LMDeals
//
//  Created by vairat on 20/06/15.
//  Copyright (c) 2015 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>


@property (strong, nonatomic) IBOutlet UITextField *searchCriteria_TextField;
@property(nonatomic,retain)IBOutlet UITextField *suburb_TextField;
@property(nonatomic,retain)IBOutlet UITextField *keyword_TextField;
@property (strong, nonatomic) IBOutlet UITableView *menuTableView;


-(IBAction)findMeADeal_Pressed:(id)sender;

@end
