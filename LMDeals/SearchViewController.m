//
//  SearchViewController.m
//  LMDeals
//
//  Created by vairat on 20/06/15.
//  Copyright (c) 2015 vairat. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchCell.h"
#import "RevealController.h"
#import "DealsViewController.h"
#import "MyDealsViewController.h"
#import "PostADealViewController.h"
#import "MyAccountViewController.h"
#import "ShareViewController.h"
#import "HelpViewController.h"

@interface SearchViewController (){
    
    UIPickerView  *searchCriteriaPicker;
    UITextField   *current_TextField;
}
@property(nonatomic,retain)NSArray *searchCriteriaItems;
@end

@implementation SearchViewController
@synthesize menuTableView, searchCriteriaItems;
@synthesize searchCriteria_TextField, keyword_TextField, suburb_TextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.searchCriteriaItems = [[NSArray alloc]initWithObjects:@"Automotive",@"Dining & FastFood",@"Golf Courses",@"Healthy & Beauty",@"Home & lifestyle",@"Leisure & Entertainment",@"Shopping & Vochers",@"Travel & Accommodation", nil];
    
    searchCriteriaPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0,40, 320, 216)];
    searchCriteriaPicker.delegate = self;
    searchCriteriaPicker.showsSelectionIndicator = YES;
    [searchCriteriaPicker setFrame:CGRectMake(0, 50, 320, 250)];
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapAction:)];
    //[self.view addGestureRecognizer:tapRecognizer];
}

- (void)onTapAction:(UITapGestureRecognizer*)recognizer
{
    
    [current_TextField resignFirstResponder];

}


-(IBAction)findMeADeal_Pressed:(id)sender{
    
    [current_TextField resignFirstResponder];
    
    RevealController *revealController = [self.parentViewController isKindOfClass:[RevealController class]] ? (RevealController *)self.parentViewController : nil;
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    DealsViewController *dealsViewController = [storyboard instantiateViewControllerWithIdentifier:@"DealsViewController"];
    
    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:dealsViewController];
    [revealController setFrontViewController:navigationController animated:NO];
    
    
    
    
}










//==================------------------- TABLEVIEW DELEGATE METHODS --------------=============================//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0)
        return 2;
    else
        return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    static NSString *CellIdentifier = @"Cell";
    SearchCell *cell = (SearchCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SearchCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == 0)
    {
        if(indexPath.row == 0){
            cell.cellNameLabel.text = @"My Deals";
            cell.cellImageView.image = [UIImage imageNamed:@"MyDeals.png"]; }
        else{
            cell.cellNameLabel.text = @"Post a Deal";
            cell.cellImageView.image = [UIImage imageNamed:@"PostADeal.png"];}
    }
    else  {
        
        if(indexPath.row == 0){
            cell.cellNameLabel.text = @"My Account";
            cell.cellImageView.image = [UIImage imageNamed:@"MyAccount.png"];}
        else if(indexPath.row == 1){
            cell.cellNameLabel.text = @"Share this app with friends";
            cell.cellImageView.image = [UIImage imageNamed:@"Share.png"];}
        else{
            cell.cellNameLabel.text = @"Help";
            cell.cellImageView.image = [UIImage imageNamed:@"help.png"];}
    }
    
    return cell;
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    return 50;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIImage *myImage;
    if(section == 0)
        myImage = [UIImage imageNamed:@"dealsec1.png"];
    else
        myImage = [UIImage imageNamed:@"othersec1.png"];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:myImage] ;
    return imageView;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if(section == 0)
        return @"Deals";
    else
        return @"Others";
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
            NSLog(@"didSelectRowAtIndexPath");
    
           RevealController *revealController = [self.parentViewController isKindOfClass:[RevealController class]] ? (RevealController *)self.parentViewController : nil;
           UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
           UINavigationController *navigationController;
    
    if(indexPath.section == 0)
    {
        
        switch (indexPath.row) {
            case 0:{
                     MyDealsViewController *myDealViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyDealsViewController"];
                
                     navigationController = [[UINavigationController alloc] initWithRootViewController:myDealViewController];
                   }
            break;
                
            case 1:{
                     PostADealViewController *postDealViewController = [storyboard instantiateViewControllerWithIdentifier:@"PostADealViewController"];
                
                     navigationController = [[UINavigationController alloc] initWithRootViewController:postDealViewController];
                   }
            break;
                
            default:
                break;
        }
        
    }
    else{
        switch (indexPath.row) {
            case 0:{
                     MyAccountViewController *myAccountViewController = [storyboard instantiateViewControllerWithIdentifier:@"MyAccountViewController"];
                
                     navigationController = [[UINavigationController alloc] initWithRootViewController:myAccountViewController];
                   }
            break;
            case 1:{
                    ShareViewController *shareViewController = [storyboard instantiateViewControllerWithIdentifier:@"ShareViewController"];
                
                    navigationController = [[UINavigationController alloc] initWithRootViewController:shareViewController];
                   }
            break;
            case 2:{
                     HelpViewController *helpViewController = [storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
                
                     navigationController = [[UINavigationController alloc] initWithRootViewController:helpViewController];
                   }
                break;
            default:
                break;
        }
        
        
        
    }
    [revealController setFrontViewController:navigationController animated:NO];
    
}

//======================------------------TEXTFIELD DELEGATE METHODS-------------------===========================//

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    current_TextField = textField;
    if(textField.tag == 1){
        textField.inputView = searchCriteriaPicker;
        [searchCriteriaPicker selectRow:4 inComponent:0 animated:NO];
    }
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}
//================================---------------UIPICKERVIEW METHODS----------------====================================//

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return [self.searchCriteriaItems count];
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    
    return [self.searchCriteriaItems objectAtIndex:row];
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    
    
    self.searchCriteria_TextField.text = [self.searchCriteriaItems objectAtIndex:row];
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
