//
//  DealsTableCell.m
//  LMDeals
//
//  Created by vairat on 27/06/15.
//  Copyright (c) 2015 vairat. All rights reserved.
//

#import "DealsTableCell.h"

@implementation DealsTableCell
@synthesize productNameLabel, productImgView;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
