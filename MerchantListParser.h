//
//  MerchantListParser.h
//  GrabItNow
//
//  Created by Monish Kumar on 14/01/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@protocol MerchantXMLParserDelegate;

@interface MerchantListParser : NSObject <NSXMLParserDelegate> {
    __unsafe_unretained id <MerchantXMLParserDelegate> delegate;
}

@property (unsafe_unretained) id <MerchantXMLParserDelegate> delegate;

@end


@protocol MerchantXMLParserDelegate <NSObject>
- (void) parsingMerchantListFinished:(NSArray *) merchantsListLocal;
- (void) parsingMerchantListXMLFailed;
@end

