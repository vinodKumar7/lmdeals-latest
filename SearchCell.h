//
//  SearchCell.h
//  MyARSample
//
//  Created by vairat on 04/09/13.
//  Copyright (c) 2013 vairat. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface SearchCell : UITableViewCell{
    
}
@property (nonatomic, strong) IBOutlet UILabel *cellNameLabel;
@property (nonatomic, strong) IBOutlet UIImageView *cellImageView;
@property (nonatomic, strong) IBOutlet UIButton *cellButton;
@end
