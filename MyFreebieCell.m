//
//  MyFreebieCell.m
//  CoffeeApp
//
//  Created by vairat on 15/06/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import "MyFreebieCell.h"

@implementation MyFreebieCell
@synthesize cellBackground_View;
@synthesize freebie_ImageView;
@synthesize productName_Label;

//- (void)awakeFromNib {
//    // Initialization code
//}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
@end
